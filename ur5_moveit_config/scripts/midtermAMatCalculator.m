%% Init Workspace
clear;

%% Define linkages
% Length of all the linkages to the end effector
l = [0.089159, 0.425, 0.39225, 0.10915, 0.09465, 0.0823];

%% Pose 1
% q derived from running the get_data.py script after pose 1
q_1 = [-2.9293389360456223, -2.5196728531508943, 0.5272330408347741, 1.993367510657569, -1.3594583253717525, 3.140913470911021];

A1 = UR5_A_Calc(q_1,l);

%% Pose 2
q_2 = [-2.7906481718541327, 4.794011369612547, -2.209429114743048, 0.5566937334872647, 1.2190103442690752, -6.282312763075002];

A2 = UR5_A_Calc(q_2,l);

%% Pose 3
q_3 = [-2.790648171860866, 4.292244440672061, -2.39724686182209, 1.2462781353341121, 1.2190103443018083, -6.282312603705379];
A3 = UR5_A_Calc(q_3,l);

%% Let's also calculate the Jacobian in Matlab 
% because I really don't want to do that by hand....

% Define q1 -> q6 as variables
syms q1 q2 q3 q4 q5 q6

% Determine A symbolically
q  =[q1 q2 q3 q4 q5 q6];
A = UR5_A_Calc(q,l);

x = A(1,4);
y = A(2,4);
z = A(3,4);
pitch = atan2(-A(3,1),sqrt(A(1,1)^2+A(2,1)^2));
yaw = atan2(A(2,1)/cos(pitch),A(1,1)/cos(pitch));
roll = atan2(A(3,2)/cos(pitch),A(3,3)/cos(pitch));

%% Calculate the Jacobian

J = jacobian([x,y,z,roll,pitch,yaw],q);

%% Calculate the Jacobian at each pose:
% Pose 1
Jnew_sub1 = subs(J,[q1,q2,q3,q4,q5,q6],q_1);
Jnew1 = zeros(6); % Creating a 6x6 Matrix
% Use a for loop to calculate the value of the final substituted jacobian
% matrix since the subs command does not evaluate for sin and cos
for i = 1:6
    for j = 1:6
        Jnew1(i,j) = double(Jnew_sub1(i,j));
    end
end

% Pose 2
Jnew_sub2 = subs(J,[q1,q2,q3,q4,q5,q6],q_2);
Jnew2 = zeros(6); % Creating a 6x6 Matrix
% Use a for loop to calculate the value of the final substituted jacobian
% matrix since the subs command does not evaluate for sin and cos
for i = 1:6
    for j = 1:6
        Jnew2(i,j) = double(Jnew_sub2(i,j));
    end
end

% Pose 3
Jnew_sub3 = subs(J,[q1,q2,q3,q4,q5,q6],q_3);
Jnew3 = zeros(6); % Creating a 6x6 Matrix
% Use a for loop to calculate the value of the final substituted jacobian
% matrix since the subs command does not evaluate for sin and cos
for i = 1:6
    for j = 1:6
        Jnew3(i,j) = double(Jnew_sub3(i,j));
    end
end

