%% This function takes in all the input values f
function A = UR5_A_Calc(q,l)

    % Build the UR5 DH Table
    theta = [q(1) q(2) q(3) q(4) q(5) q(6)];
    d = [l(1) 0 0 l(4) l(5) l(6)];
    a = [0 l(2) l(3) 0 0 0];
    alpha = [pi/2 0 0 pi/2 pi/2 0];

    % Define A as the identiy matrix to start. This will become the A matrix
    A = eye(4);

    % Calculate the A matrix
    for i = 1:length(l)
        T = [cos(theta(i)),-cos(alpha(i))*sin(theta(i)),sin(alpha(i))*sin(theta(i)),a(i)*cos(theta(i));
            sin(theta(i)),cos(theta(i))*cos(alpha(i)),-sin(alpha(i))*cos(theta(i)), a(i)*sin(theta(i));
            0,sin(alpha(i)),cos(alpha(i)),d(i);
            0,0,0,1];
        A = A*T;
    end

end